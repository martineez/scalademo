name := "scalademo"

version := "1.0"

scalaVersion := "2.12.1"

libraryDependencies ++= Seq(
    "org.specs2" %% "specs2-core" % "3.8.6" % "test",
    "org.scalatest" %% "scalatest" % "3.0.1" % "test",
    "junit" % "junit" % "4.12" % "test",
    "com.typesafe.akka" %% "akka-actor" % "2.4.14",
    "com.typesafe.akka" %% "akka-testkit" % "2.4.14"
)

resolvers ++= Seq(
    "snapshots" at "http://oss.sonatype.org/content/repositories/snapshots",
    "releases"  at "http://oss.sonatype.org/content/repositories/releases",
    "maven"     at "http://repo1.maven.org/maven2"
)

resolvers ++= Seq("snapshots", "releases").map(Resolver.sonatypeRepo)