package specs2

import org.specs2.mutable._

/**
 * Created by martin on 12.7.13.
 * Unit specifications extend the org.specs2.mutable.Specification trait
 * and are using the should/in format:
 */
class HelloWorldSpec1 extends Specification {

  "The 'Hello world' string" should {
    "contain 11 characters" in {
      "Hello world" must have size(11)
    }
    "start with 'Hello'" in {
      "Hello world" must startWith("Hello")
    }
    "end with 'world'" in {
      "Hello world" must endWith("world")
    }
  }
}
