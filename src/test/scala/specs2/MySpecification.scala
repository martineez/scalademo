package specs2
import org.specs2.Specification
import org.specs2.matcher.StandardMatchResults.ok
import org.specs2.specification.Grouped
import org.specs2.specification.create.S2StringContextCreation

class MySpecification extends Specification with Examples { def is =  s2"""
  first example in first group                                        ${g1.e1}
  second example in first group                                       ${g1.e2}

  first example in second group                                       ${g2.e1}
  second example in second group                                      ${g2.e2}
  third example in second group, not yet implemented                  ${g2.e3}
  """
}

trait Examples extends Grouped with S2StringContextCreation {
  // group of examples with no description
  new g1 {
    e1 := ok
    e2 := ok
  }
  // group of examples with a description for the group
  "second group of examples" - new g2 {
    e1 := ok
    e2 := ok
  }
}
