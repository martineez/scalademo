import org.scalatest.FunSuite

/**
  * http://alvinalexander.com/scala/scala-list-class-examples
  *
  */
class ListTest extends FunSuite {

  test("create a list") {
    val list = 1 :: 2 :: 3 :: Nil

    assert(list === List(1, 2, 3))
  }

  test("create a range") {
    val list = List.range(1, 10)

    assert(list === List(1, 2, 3, 4, 5, 6, 7, 8, 9))
  }

  test("create a range 2") {
    val list = List.range(0, 10, 2)

    assert(list === List(0, 2, 4, 6, 8))
  }

  test("fill list") {
    val list = List.fill(3)("foo")

    assert(list === List("foo", "foo", "foo"))
  }

  test("tabulate list") {
    val list = List.tabulate(5)(n => n * n)

    assert(list === List(0, 1, 4, 9, 16))
  }

  test("prepend list") {
    val list = List(1, 2, 3)
    val prep = 0 :: list

    assert(prep === List(0, 1, 2, 3))
  }

  test("merge two lists") {
    val a = List(1, 2, 3)
    val b = List(4, 5, 6)
    val result = (1 to 6).toList

    assert(a ::: b === result)
    assert(List.concat(a, b) === result)
    assert(a ++ b === result)
  }

  test("sum list") {
    var ∑ = 0
    val list = List(1, 2, 3)
    list.foreach(∑ += _)

    assert(∑ == 6)
  }

  def forloop() {
    val names = List("Bob", "Fred", "Joe", "Julia", "Kim")
    for (name <- names) println(name)

    for (name <- names if name.startsWith("J"))
      println(name)
  }

  test("filter list") {
    val list = List.range(1, 10)
    val evens = list.filter(a => a % 2 == 0)

    assert(evens === List(2, 4, 6, 8))
  }

  test("takeWhile list") {
    val list = List.range(1, 10)
    val result = list.takeWhile(a => a < 6)

    assert(result === List(1, 2, 3, 4, 5))
  }

  test("dropWhile list") {
    val list = List.range(1, 10)

    assert(list.dropWhile(a => a < 6) === List(6, 7, 8, 9))
  }

  test("span list") {
    val list = List.range(1, 10)

    assert(list.span(a => a < 6) ===(
      List(1, 2, 3, 4, 5),
      List(6, 7, 8, 9))
    )
  }

  test("map list") {
    val list = List(1, 2, 3)

    assert(list.map(a => a * 2) === List(2, 4, 6))
    assert(list.map(_ * 2) === List(2, 4, 6))
  }

  test("toLower toUpper") {
    val names = List("Fred", "Joe", "Bob")
    val lower = names.map(_.toLowerCase)
    val upper = names.map(_.toUpperCase)

    assert(lower === List("fred", "joe", "bob"))
    assert(upper === List("FRED", "JOE", "BOB"))
  }

  test("map list 2") {
    val names = List("Fred", "Joe", "Bob")
    val li = names.map(name => <li>{name}</li>)

    assert(li === List(<li>Fred</li>, <li>Joe</li>, <li>Bob</li>))
  }

  test("sort list") {
    val list = List(10, 2, 5)
    val result = list.sortWith(_ < _)

    assert(result === List(2, 5, 10))
  }

  test("misc list") {
    val list = List.range(1, 10)

    assert(list.length === 9)
    assert(list.isEmpty == false)
    assert(list.head === 1)
    assert(list.last === 9)
    assert(list.tail === List(2, 3, 4, 5, 6, 7, 8, 9))
    assert(list.init === List(1, 2, 3, 4, 5, 6, 7, 8))
    assert(list.reverse === List(9, 8, 7, 6, 5, 4, 3, 2, 1))
    assert(list.mkString(",") === "1,2,3,4,5,6,7,8,9")
  }

  test("partition list") {
    val list = List.range(1, 10)
    val (even, odd) = list partition (_ % 2 == 0)

    assert(odd === List(1, 3, 5, 7, 9))
    assert(even === List(2, 4, 6, 8))
  }

}