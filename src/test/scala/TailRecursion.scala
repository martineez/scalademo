import org.scalatest.FunSuite

import scala.annotation.tailrec

object TailRecursion {

  // head recursion
  //@tailrec
  def listLength1(list: List[_]): Int = {
    if (list == Nil) 0
    else 1 + listLength1(list.tail)
  }


  // tail recursion
  def listLength2(list: List[_]): Int = {
    @tailrec
    def listLength2Helper(list: List[_], len: Int): Int = {
      if (list == Nil) len
      else listLength2Helper(list.tail, len + 1)
    }
    listLength2Helper(list, 0)
  }
}

class TailRecursionSpec extends FunSuite {

  test("merge two lists") {
    val list1 = (1 to 3).toList
    val list2 = (4 to 6).toList

    assert(list1 ++ list2 == (1 to 6).toList)
  }

  test("tail recursion") {
    val list1 = (1 to 10).toList
    var list2 = (1 to 10).toList

    1 to 15 foreach (x => list2 = list2 ++ list2)

    assert (10 === TailRecursion.listLength1(list1))
    intercept[java.lang.StackOverflowError] {
      assert(10 === TailRecursion.listLength1(list2))
    }
    assert (10 === TailRecursion.listLength2(list1))
    assert (327680 === TailRecursion.listLength2(list2))
  }
}
