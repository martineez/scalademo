import scala.util.Random

import org.scalatest.FunSuite

class SortTest extends FunSuite {

  val Sorted = List.range(1, 500000)
  val Shuffled = Random.shuffle(Sorted)

  test("array sort") {
    val arr = Shuffled.toArray
    Sort.sort_like_c(arr)

    assert(Sorted.toArray === arr)
  }

  test("list sort scala library") {
    val result = Shuffled.sorted
    assert(Sorted === result)
  }

  test("list sort1") {
    val result = Sort.sort1(Shuffled)
    assert(Sorted === result)
  }

  test("list sort2") {
    val result = Sort.sort2(Shuffled)
    assert(Sorted === result)
  }

  test("pending not yet implemented") (pending)

}
