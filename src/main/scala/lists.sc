object lists {
  val list = List(1, 2, 3, 4, 5)                  //> list  : List[Int] = List(1, 2, 3, 4, 5)

  def sum(xs: List[Int]) = (xs foldLeft 0)(_ + _) //> sum: (xs: List[Int])Int
  def product(xs: List[Int]) = (xs foldLeft 1)(_ * _)
                                                  //> product: (xs: List[Int])Int
  sum(list)                                       //> res0: Int = 15
  product(list)                                   //> res1: Int = 120
}