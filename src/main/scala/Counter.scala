/**
  * Created by martin on 16.02.14.
  */
object Counter {
  def main(args: Array[String]) {
    var src = scala.io.Source.fromFile("src/main/scala/Counter.scala")
    val count = src.getLines().map(x => 1).sum
    println(count)
  }
}
