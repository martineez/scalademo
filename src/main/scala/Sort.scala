object Sort {
  def sort_like_c(xs: Array[Int]) {
    def swap(i: Int, j: Int) {
      val t = xs(i)
      xs(i) = xs(j)
      xs(j) = t
    }

    def sort1(l: Int, r: Int) {
      val pivot = xs((l + r) / 2)
      var i = l
      var j = r
      while (i <= j) {
        while (xs(i) < pivot) i += 1
        while (xs(j) > pivot) j -= 1
        if (i <= j) {
          swap(i, j)
          i += 1
          j -= 1
        }
      }
      if (l < j) sort1(l, j)
      if (j < r) sort1(i, r)
    }
    sort1(0, xs.length - 1)
  }


  def sort1[A <% Ordered[A]](xs: List[A]): List[A] = {
    if (xs.length <= 1) xs
    else {
      val pivot = xs(xs.length / 2)
      List.concat(
        sort1(xs filter (pivot >)),
        xs filter (pivot ==),
        sort1(xs filter (pivot <)))
    }
  }

  // https://dzone.com/articles/a-beginner-friendly-tour-through-functional-progra
  def sort2[A <% Ordered[A]](as: List[A]): List[A] = as match {
    case Nil => Nil
    case a :: as =>
      val (before, after) = as partition (_ < a)
      sort2(before) ++ (a :: sort2(after))
  }

}
