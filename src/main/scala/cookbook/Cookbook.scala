package cookbook

class Recipe(
  val ingredients: List[String] = List.empty,
  val directions: List[String] = List.empty) {
  println("Ingredients: " + ingredients)
  println("Directions: " + directions)
}

class Cookbook(val recipes: Map[String, Recipe]) {
  println("Recipes: " + recipes)

  def this() = this(Map.empty)
}

object Main {
  def main(args: Array[String]): Unit = {
    val recipe = new Recipe(
      List("peanut butter", "jelly", "bread"),
      List("put the")
    )
  }
}