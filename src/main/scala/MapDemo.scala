object MapDemo {
  val seq = Seq(1, 2, 3, 4, 5)

  def main(args: Array[String]): Unit = {
    println(seq.map(math.pow(2, _)))
  }
}
