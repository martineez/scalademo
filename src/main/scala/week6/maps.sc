package week6

object maps {
  val romanNumerals = Map("I" -> 1, "V" -> 5, "X" -> 10)
                                                  //> romanNumerals  : scala.collection.immutable.Map[String,Int] = Map(I -> 1, V -
                                                  //| > 5, X -> 10)
  val capitalOfCountry = Map("US" -> "Washington", "Switzerland" -> "Bern")
                                                  //> capitalOfCountry  : scala.collection.immutable.Map[String,String] = Map(US -
                                                  //| > Washington, Switzerland -> Bern)

  capitalOfCountry("US")                          //> res0: String = Washington
  //capitalOfCountry("Andorra")

  def showCapital(country: String) = capitalOfCountry.get(country) match {
    case Some(capital) => capital
    case None => "missing data"
  }                                               //> showCapital: (country: String)String

  showCapital("Andorra")                          //> res1: String = missing data

  capitalOfCountry get "Andorra"                  //> res2: Option[String] = None
  capitalOfCountry get "US"                       //> res3: Option[String] = Some(Washington)
}