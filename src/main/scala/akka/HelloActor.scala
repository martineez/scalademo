package akka

import akka.actor._

class HelloActor extends Actor {
  override def receive = {
    case "hello" => println("hello back to you")
    case _ => println("huh?")
  }
}

object Main extends App {
  val system = ActorSystem("HelloSystem")
  val helloActor = system.actorOf(Props[HelloActor], name = "helloactor")

  helloActor ! "hello"
  helloActor ! "buenos dias"
}