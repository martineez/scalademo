import scala.xml._
import java.net._
import scala.io.Source
object weather {
  val theUrl = "http://weather.yahooapis.com/forecastrss?w=12770744&u=f"
                                                  //> theUrl  : String = http://weather.yahooapis.com/forecastrss?w=12770744&u=f

  val xmlString = Source.fromURL(new URL(theUrl)).mkString
                                                  //> xmlString  : String = "<?xml version="1.0" encoding="UTF-8" standalone="yes"
                                                  //|  ?>
                                                  //| 		<rss version="2.0" xmlns:yweather="http://xml.weather.yahoo.com/
                                                  //| ns/rss/1.0" xmlns:geo="http://www.w3.org/2003/01/geo/wgs84_pos#">
                                                  //| 			<channel>
                                                  //| 		
                                                  //| <title>Yahoo! Weather - Atlanta, GA</title>
                                                  //| <link>http://us.rd.yahoo.com/dailynews/rss/weather/Atlanta__GA/*http://weath
                                                  //| er.yahoo.com/forecast/USGA0763_f.html</link>
                                                  //| <description>Yahoo! Weather for Atlanta, GA</description>
                                                  //| <language>en-us</language>
                                                  //| <lastBuildDate>Sun, 01 Jun 2014 6:10 am EDT</lastBuildDate>
                                                  //| <ttl>60</ttl>
                                                  //| <yweather:location city="Atlanta" region="GA"   country="United States"/>
                                                  //| <yweather:units temperature="F" distance="mi" pressure="in" speed="mph"/>
                                                  //| <yweather:wind chill="70"   direction="0"   speed="3" />
                                                  //| <yweather:atmosphere humidity="83"  visibility="10"  pressure="30.18"  risin
                                                  //| g="0" />
                                                  //| <yweather:astronomy sunrise="6:25 am"   sunset="8:40 pm"/>
                                                  //| <image>
                                                  //| <ti
                                                  //| Output exceeds cutoff limit.
  val xml = XML.loadString(xmlString)             //> xml  : scala.xml.Elem = <rss version="2.0" xmlns:geo="http://www.w3.org/2003
                                                  //| /01/geo/wgs84_pos#" xmlns:yweather="http://xml.weather.yahoo.com/ns/rss/1.0"
                                                  //| >
                                                  //| 			<channel>
                                                  //| 		
                                                  //| <title>Yahoo! Weather - Atlanta, GA</title>
                                                  //| <link>http://us.rd.yahoo.com/dailynews/rss/weather/Atlanta__GA/*http://weath
                                                  //| er.yahoo.com/forecast/USGA0763_f.html</link>
                                                  //| <description>Yahoo! Weather for Atlanta, GA</description>
                                                  //| <language>en-us</language>
                                                  //| <lastBuildDate>Sun, 01 Jun 2014 6:10 am EDT</lastBuildDate>
                                                  //| <ttl>60</ttl>
                                                  //| <yweather:location country="United States" region="GA" city="Atlanta"/>
                                                  //| <yweather:units speed="mph" pressure="in" distance="mi" temperature="F"/>
                                                  //| <yweather:wind speed="3" direction="0" chill="70"/>
                                                  //| <yweather:atmosphere rising="0" pressure="30.18" visibility="10" humidity="8
                                                  //| 3"/>
                                                  //| <yweather:astronomy sunset="8:40 pm" sunrise="6:25 am"/>
                                                  //| <image>
                                                  //| <title>Yahoo! Weather</title>
                                                  //| <width>142</width>
                                                  //| <height>18</height>

  val city = xml \\ "location" \\ "@city"         //> city  : scala.xml.NodeSeq = NodeSeq(Atlanta)
  val state = xml \\ "location" \\ "@region"      //> state  : scala.xml.NodeSeq = NodeSeq(GA)
  val temperature = xml \\ "condition" \\ "@temp" //> temperature  : scala.xml.NodeSeq = NodeSeq(70)

  println(city + ", " + state + " " + temperature)//> Atlanta, GA 70
}