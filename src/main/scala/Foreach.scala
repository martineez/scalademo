object Foreach {
  val seq = Seq(1, 2, 3, 4, 5)

  def main(args: Array[String]): Unit = {
    seq.foreach {
      element => println(element)
    }
  }
}
