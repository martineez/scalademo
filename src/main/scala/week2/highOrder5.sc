def sum(f: Int => Int): (Int, Int) => Int = {
  def sumF(a: Int, b: Int): Int =
    if (a > b) 0
    else f(a) + sumF(a + 1, b)
  sumF
}

def fact(x: Int): Int = if (x == 0) 1 else x * fact(x - 1)

def sumInts = sum(x => x)
def sumCubes = sum(x => x * x * x)
def sumFactorials = sum(fact)


sumInts(3, 5)
sumCubes(3, 5)
sumFactorials(3, 5)