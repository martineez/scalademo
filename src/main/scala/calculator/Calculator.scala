package calculator

import scala.collection.mutable

object Calculator {

  def handleOperator(token: String, stack: mutable.Stack[Int]): Boolean = token match {
    case "+" =>
      val rhs = stack.pop()
      val lhs = stack.pop()
      stack.push(lhs + rhs)
      true
    case "-" =>
      val rhs = stack.pop()
      val lhs = stack.pop()
      stack.push(lhs - rhs)
      true
    case "*" =>
      val rhs = stack.pop()
      val lhs = stack.pop()
      stack.push(lhs * rhs)
      true
    case "/" =>
      val rhs = stack.pop()
      val lhs = stack.pop()
      stack.push(lhs / rhs)
      true
    case _ => false
  }

  def handleNumber(token: String, stack: mutable.Stack[Int]): Boolean =  try {
    stack.push(token.toInt)
    true
  } catch {
    case _: NumberFormatException => false
  }

  def calculate (expression: String): Int = {
    val stack = new mutable.Stack[Int]

    // handle each token
    for (token <- expression.split(" "))
      if (!handleOperator(token, stack) && !handleNumber(token, stack))
        throw new IllegalArgumentException("invalid toke: " + token)
    stack.pop()
  }

  def main(args: Array[String]): Unit =
    if (args.length != 1) {
      // expect exactly one argument
      throw new IllegalArgumentException("Usage: Calculator <expression>")
    } else {

      println(calculate(args(0)))
    }
}
