import scala.annotation.tailrec

object Factorial extends App {
  val factorials = List(20, 18, 32, 28, 22, 42, 55, 48)

  for (num <- factorials) {
//    println(s"factorial for $num is ${factor1(num)}")
//    println(s"factorial for $num is ${factor2(num)}")
    println(s"factorial for $num is ${factor3(num)}")
  }

  private def factor1(num: Int): BigInt = {
    num match {
      case 0 => 1
      case n => n * factor1(n - 1)
    }
  }

  private def factor2(num: Int) = factorTail(num, 1)

  @tailrec
  private def factorTail(num: Int, acc: BigInt): BigInt = {
    (num, acc) match {
      case (0, acc) => acc
      case (n, acc) => factorTail(n - 1, n * acc)
    }
  }

  def factor3(num: Int): BigInt = {
    @tailrec
    def factorTail(num: Int, acc: BigInt): BigInt =
      if (num == 0) acc
      else factorTail(num - 1, num * acc)

    factorTail(num, 1)
  }
}